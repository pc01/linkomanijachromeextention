﻿

// VERIFY INJECTION //////////////////////////////////////////////////////////////////////////////

if (window.location.href.indexOf("https://www.linkomanija.net/browse.php") != 0 )
{
    throw "LMEXT: not in browsing window";
}

function getText( obj ) {
    return obj.textContent ? obj.textContent : obj.innerText;
}

function addStyleString(str) {
    var node = document.createElement('style');
    node.innerHTML = str;
    document.head.appendChild(node);
}
function addBodyString(nm, str) {
    var node = document.createElement(nm);
    node.innerHTML = str;
    document.body.appendChild(node);
}
function lerp (start, end, amt){
	return (1-amt)*start+amt*end
}
function animate(start,end,interval,totalSteps,editor){
	var stepsLeft = totalSteps;
	var animate;
	step();
	
	function step() {
	   editor(lerp(parseFloat(start), parseFloat(end), 1 - stepsLeft / totalSteps ))
	   stepsLeft--;
	   if (stepsLeft >= 0) animate = setTimeout(step,interval);
	   else clearTimeout(animate);
	}
}
//addStyleString("/*TheModal(background)*/.modal{display:none;/*Hiddenbydefault*/position:fixed;/*Stayinplace*/z-index:1;/*Sitontop*/padding-top:100px;/*Locationofthebox*/left:0;top:0;width:100%;/*Fullwidth*/height:100%;/*Fullheight*/overflow:auto;/*Enablescrollifneeded*/background-color:rgb(0,0,0);/*Fallbackcolor*/background-color:rgba(0,0,0,0.4);/*Blackw/opacity*/}/*ModalContent*/.modal-content{background-color:#fefefe;margin:auto;padding:20px;border:1pxsolid#888;width:80%;}/*TheCloseButton*/.close{color:#aaaaaa;float:right;font-size:28px;font-weight:bold;}.close:hover,.close:focus{color:#000;text-decoration:none;cursor:pointer;}");
//addBodyString('span', "<!-- The Modal --><div id='myModal' class='modal'>  <!-- Modal content -->  <div class='modal-content'>    <span class='close'>&times;</span>    <span id='modalSpan'>Some text in the Modal..</span>  </div></div>");
//addBodyString('script', `function showIframeModal(btn, lnk){		var modal = document.getElementById('myModal');	var modalSpan = document.getElementById('modalSpan');	modalSpan.innerHTML = "<iframe src='"+lnk+"' width='560' height='315' frameborder='0' allowfullscreen='true'></iframe>";		var span = document.getElementsByClassName("close")[0];	 	btn.onclick = function() {		modal.style.display = "block";	}		span.onclick = function() {		modal.style.display = "none";	}	window.onclick = function(event) {		if (event.target == modal) {			modal.style.display = "none";		}	}	}`);




//
var genreList=[ 
['Animation','Animacija','Animacinis'],
['Action','Veiksmo'],
['Adventure','Nuotykiai','Nuotykių'],
['Biography','Biografija','Biografinis'],
['Crime','Kriminalinis','Kriminalas'],
['Comedy','Komedija'],
['Drama'],
['Documentary','Dokumentika'],
['Family','Šeimai'],
['Fantasy','Fantastinis','Fantastika'],
['Horror','Siaubo'],
['History','Istorinis'],
['Mystery','Mistinis','Mistika'],
['Music','Musical','Miuziklas','Muzikinis'],
['News'],
['Romance','Romantinis'],
['Short'],
['Sport','Sportas'],
['Sci-Fi','Mokslinė','fantastika'],
['Thriller','Trileris'],
['Talk-Show'],
['War','Karinis','Karas'],
['Western','Vesternas']
];
var genreListEnb=[];

var txtExt = ["Rating:", "Country:", "Language:", "Premjera:","Šalis:"];
var genreExt = ["Genres:", "Genre:", "Žanras:","Žanrai:"];
function manageCallback(td, xml, mnurl,td2){
	var tr=xml.getElementsByClassName("descr_text");
	var src=tr[0].getElementsByTagName("img");
	if(src.length>0){
		src=src[0];
		if(src!=null){
			src=src.src;			
		}
	}
	else {
		src='https://semantic-ui.com/images/wireframe/image.png';
	}
	
	var bs=tr[0].childNodes;
	
	var keep=true;
	
	for(var b=0;b<bs.length;b++){
						
		if(bs[b].innerHTML=="IMDb:"){
			td.appendChild(document.createElement('BR'));
			bs[b+2].innerText="IMDB";
			td.appendChild(bs[b+2]);
			b++;
		} else
		if(genreExt.indexOf(bs[b].innerHTML)>=0){
			
			keep=localStorage.getItem('gviewEnabled')=="0";
			var gs=getText( bs[b+1] ).split(/[, \/]+/);
			//console.log( gs+"  "+gs.length );
			
			if(!keep)
			for(var g=0;g<gs.length;g++){
				
				if(gs[g].length>3){
					gs[g]=gs[g].trim();
					
					//console.log( gs[g] );
					
					var fnd=false;
					for(var gl=0;gl<genreList.length;gl++){
						if(genreList[gl].indexOf(gs[g])>=0){
							fnd=true;							
							if(genreListEnb[gl])
								keep=true;
							break;						
						}
					}
					if(!fnd){
						var arr=[ gs[g] ];
						genreList.push( arr );
						genreTable.appendChild( document.createTextNode( gs[g]+"," ) );
						keep=true;
					}
				}
			}

			
			td.appendChild(document.createElement('BR'));
			td.appendChild(bs[b]);
			td.appendChild(bs[b]);
			b--;
		} else
		if(txtExt.indexOf(bs[b].innerHTML)>=0){
			td.appendChild(document.createElement('BR'));
			td.appendChild(bs[b]);
			td.appendChild(bs[b]);
			b--;
		}
			
	}
	
	if(keep){
		var a = document.createElement('a');
		

		const imgTemp = new Image();

		var img=document.createElement("IMG");
		
		img.style.width = '0px';
		img.style.height = '0px';

		var initialHeight = 0;
		var initialWidth = 0;

		img.onload = function(){
			var mm=Math.max(initialHeight,initialWidth)+0.01;
			var aa=mm+0.01==initialHeight? 400.0: 275.0;
			var mx=(aa*((initialWidth+0.01)/mm));
			var my=(aa*((initialHeight+0.01)/mm));

			//console.log(src+' laoded2: '+mx+','+my);
			
			var observer = new IntersectionObserver(function(entries) {
				if(entries[0].isIntersecting === true){
					animate(0, mx, 10, 15, lerpValue => img.style.width = lerpValue+'px');
					animate(0, my, 10, 15, lerpValue => img.style.height = lerpValue+'px');
					animate(0, 1, 10, 15, lerpValue => img.style.opacity = lerpValue);
					observer.disconnect();
				}
			}, { threshold: [1] });
			observer.observe(img);

			//td2.appendChild( document.createTextNode( img.width+"x"+img.height+"->"+mx+"x"+my ) );

		}

		imgTemp.onload = function() {
			initialHeight = this.height;
			initialWidth = this.width;

			//console.log(src+' laoded: '+initialHeight+','+initialWidth);

			img.src=src;
		}
		imgTemp.src=src;	

		a.style.height = 275+'px';
		a.style.width = 275+'px';
			
		a.appendChild(img);
		a.href = mnurl;
		a.target="_blank";
		td2.appendChild(a);
		
		td.appendChild(document.createElement('BR'));
		
		var iframe = xml.getElementsByTagName('iframe');
		if (iframe.length>0){
			var atrailer = document.createElement('a');
			atrailer.href = iframe[0].src;
			atrailer.target = "_blank";
			//atrailer.setAttribute("onclick", "showIframeModal(this, '"+iframe[0].src+"');");
			atrailer.innerText = 'Trailer';
			td.appendChild(atrailer);
		}
		
		td2.appendChild(document.createElement('BR'));
		
	}
	else {
		td.parentElement.removeChild(td);
		td2.parentElement.removeChild(td2);		
	}
}

function getHtml( url, callback, td, td2 ) {

    // Feature detection
    if ( !window.XMLHttpRequest ) return;

    // Create new request
    var xhr = new XMLHttpRequest();

    // Setup callback
    xhr.onload = function() {
        if ( callback && typeof( callback ) === 'function' ) {
            callback( td, this.responseXML, url,td2 );
        }
    }

    // Get the HTML
    xhr.open( 'GET', url );
    xhr.responseType = 'document';
    xhr.send();

};

var setView = function(){
	localStorage.setItem('viewEnabled',  localStorage.getItem('viewEnabled')=="1"?"0":"1"  );
	window.location.reload(false); 
}
var setGenView = function(){
	localStorage.setItem('gviewEnabled',  localStorage.getItem('gviewEnabled')=="1"?"0":"1"  );
	window.location.reload(false); 
}
var setGenre = function(gl){
	//alert(gl+"->"+localStorage.getItem('g'+gl)+" to "+(localStorage.getItem('g'+gl)=="1"?"0":"1"));
	localStorage.setItem('g'+gl,  localStorage.getItem('g'+gl)=="1"?"0":"1"  );
}



//add change view button
var ps=document.getElementsByTagName('p');
{

	var pss = ps.length > 3 ? ps[3] : ps[1];
	var clone=pss.lastChild.cloneNode(true);
	
	pss.appendChild( document.createTextNode( "  " ) );
	pss.appendChild(clone);
	
	clone.href="#";
	clone.innerText="Įjungti/išjungti filmų filtravimą";
	clone.addEventListener('click',setGenView,false);
	
	
	clone=pss.lastChild.cloneNode(true);
	
	pss.appendChild( document.createTextNode( "  " ) );
	pss.appendChild(clone);
	
	clone.href="#";
	clone.innerText="Pakeisti vaizdą";
	clone.addEventListener('click',setView,false);
	
}
if(localStorage.getItem('viewEnabled')!="1")
	throw "LMEXT: view disabled";

//add movie genres
var catTable=document.getElementById("categories");
var genreTable=catTable.cloneNode(false);
genreTable.id="genres";
catTable.parentElement.appendChild(genreTable);
{
	var gentd=catTable.getElementsByClassName('noborder browsecat')[0];
	var gtr=null;
	if(localStorage.getItem('gviewEnabled')!="0"){
		
		for(var gl=0;gl<genreList.length;gl++){
			if(gtr==null || gtr.childElementCount>=7){
				gtr=document.createElement("TR");
				genreTable.appendChild(gtr);	
			}
			var td=gentd.cloneNode(true);
			td.childNodes[0].name="g"+gl;
			td.childNodes[0].checked=localStorage.getItem('g'+gl)!="0";
			genreListEnb.push(td.childNodes[0].checked);
			
			//console.log( td.childNodes[0].checked );
			
			td.childNodes[0].addEventListener('click', 
			function(gl){
			  return function() { setGenre(gl); } 
			}(gl), false);
				
			
			td.childNodes[1].innerText=genreList[gl][0];
			td.childNodes[1].href=null;
			gtr.appendChild(td);
		}		
		gtr=document.createElement("TR");
		genreTable.appendChild(gtr);
		
		var a=document.createElement("A");
		a.href="#";
		a.innerText="(Rodyti visus)";
		a.addEventListener('click',function(){ for(var gl=0;gl<genreList.length;gl++)localStorage.setItem('g'+gl,'1');window.location.reload(false); },false); 
		gtr.appendChild(a);
		
		a=document.createElement("A");
		a.href="#";
		a.innerText="(Slėpti visus)";
		a.addEventListener('click',function(){ for(var gl=0;gl<genreList.length;gl++)localStorage.setItem('g'+gl,'0');window.location.reload(false);  },false); 
		gtr.appendChild(a);
		
		a=document.createElement("A");
		a.href="#";
		a.innerText="(Vykdyti)";
		a.addEventListener('click',function(){ window.location.reload(false);  },false);
		gtr.appendChild(a);
		
	}
}


var perRow=7;//document.getElementById('content').offsetWidth/300;

var tables = document.getElementsByTagName('table');
for(var i=0;i<tables.length;i++) {
	//console.log(tables[i]);
}
var tbl3 = tables[tables.length - 1]//tables[3];//tables.length > 3 ? tables[4] : 
var parent=tbl3.parentElement;


var newTable = document.createElement("DIV");
parent.appendChild(newTable);

var listTable = document.createElement("TABLE");



var trs=tbl3.getElementsByTagName('tr');
var tr=null;
var tr2=null;

//add sorting texts
{
	newTable.appendChild(trs[0]);
}

//add torrents
newTable.appendChild(listTable);
for(var i=0;i<trs.length;i++){
	//console.log(trs[i]);
	
	if(tr==null || tr.childElementCount>=perRow){
		tr2=document.createElement("TR");
		listTable.appendChild(tr2);		
		tr=document.createElement("TR");
		listTable.appendChild(tr);		
	}	
	var td=document.createElement("TD");
	td.style.verticalAlign="top";
	tr.appendChild(td);	
	var td2=document.createElement("TD");
	td2.className="center";
	td2.style.width='275px';
	td2.style.height='275px';
	tr2.appendChild(td2);
	
	var tds=trs[i].getElementsByTagName('td');
	
	//if (tds.length <= 1) continue;
	
	//title
	var url=tds[1].childNodes[0].href;
	td.appendChild( tds[1].childNodes[0] );
	td.appendChild( tds[1].childNodes[0] );
	td.appendChild( tds[1].childNodes[0] );
	//files
	td.appendChild(document.createElement('BR'));
	//td.appendChild( document.createTextNode( "F:" ) );
	//td.appendChild( tds[2].childNodes[0] );
	//size
	td.appendChild( document.createTextNode( "  " ) );
	td.appendChild( tds[5].childNodes[0] );
	td.appendChild( tds[5].childNodes[1] );
	//dlding
	var img=document.createElement("IMG");
	img.src='https://static.linkomanija.net/images/ul.gif';
	td.appendChild( document.createTextNode( "  " ) );
	td.appendChild( tds[7].childNodes[0] );
	td.appendChild( img );
	//leeches
	var img=document.createElement("IMG");
	img.src='https://static.linkomanija.net/images/dn.gif';
	td.appendChild( document.createTextNode( "  " ) );
	td.appendChild( tds[8].childNodes[0] );
	td.appendChild( img );
	//dls
	td.appendChild( document.createTextNode( "  DL:" ) );
	td.appendChild( tds[6].childNodes[0] );
	//comments
	td.appendChild( document.createTextNode( "  K:" ) );
	td.appendChild( tds[3].childNodes[0] );
	//type
	td.appendChild( document.createTextNode( "  " ) );
	tds[0].childNodes[0].childNodes[0].style.height = '20px';
	tds[0].childNodes[0].childNodes[0].style.width = '20px';
	td.appendChild( tds[0].childNodes[0] );
		
	getHtml(url,manageCallback,td,td2);

}


parent.replaceChild(newTable, tbl3);
